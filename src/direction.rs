use strum::EnumIter;

#[derive(PartialEq, Clone, Copy, Debug, EnumIter)]
pub enum Direction {
    Left, 
    Up, 
    Right, 
    Down, 
}
