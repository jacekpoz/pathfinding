use crate::cell::Cell;
use crate::direction::Direction;
use crate::error::Error;
use crate::pathfinder::Pathfinder;
use crate::pos::Pos;

use strum::IntoEnumIterator;

// first thing that came to mind without searching for anything
#[derive(Default)]
pub struct FirstThought {
    maze: Vec<Vec<Cell>>, 
    maze_cache: Vec<Vec<Cell>>, 
    solution_cache: Vec<Pos>, 
}

impl Pathfinder for FirstThought {
    fn new(maze: &Vec<Vec<Cell>>) -> Self {
        Self { maze: maze.clone(), ..Default::default() }
    }

    fn set_maze(&mut self, maze: Vec<Vec<Cell>>) {
        self.maze_cache = self.maze.clone();
        self.maze = maze;
    }

    fn get_maze(&self) -> &Vec<Vec<Cell>> {
        &self.maze
    }

    fn solve(&mut self) -> Result<Vec<Pos>, Error> {

        if self.maze == self.maze_cache {
            return Ok(self.solution_cache.clone());
        }

        let mut start_assigned = false;
        let mut start: Pos = Pos::invalid();
        let mut end_assigned = false;
        let mut end: Pos = Pos::invalid();
        for (y, row) in self.maze.iter().enumerate() {
            for (x, cell) in row.iter().enumerate() {
                if *cell == Cell::Start {
                    if start_assigned { return Err(Error::TooManyStarts); }
                    start = Pos { x: x as i32, y: y as i32 };
                    start_assigned = true;
                }
                if *cell == Cell::End {
                    if end_assigned { return Err(Error::TooManyEnds); }
                    end = Pos { x: x as i32, y: y as i32 };
                    end_assigned = true;
                }
            }
        }

        if start.x == i32::min_value() {
            return Err(Error::NoStart);
        }
        if end.x == i32::min_value() {
            return Err(Error::NoEnd);
        }

        let mut current_cell: Pos = start;
        let mut crossroads: Vec<Pos> = vec![current_cell];
        let mut steps: u32 = 1;
        let mut path: Vec<Pos> = vec![];
        let mut failed: Vec<Pos> = vec![];
        let mut avail_dirs: u8 = 0;
        
        loop {
            if crossroads.len() == 0 { return Err(Error::NoPath); }
            if current_cell == end { break; }

            path.push(current_cell);

            let mut next_cell = current_cell;
            // left-most direction if that makes sense
            let mut dir_chosen = false;
            for direction in Direction::iter() {
                let next = current_cell + direction;
                if self.is_empty(next) && !path.contains(&next) && !failed.contains(&next) {
                    avail_dirs += 1;
                    if !dir_chosen {
                        steps += 1;
                        next_cell = next;
                        dir_chosen = true;
                    }
                }
            }
            // no way to go -> go back to last crossroad
            // and go the next direction
            if avail_dirs == 0 {
                let mut fail = Pos::invalid();
                for _ in 0..=steps { fail = path.pop().unwrap(); }
                failed.push(fail);

                current_cell = crossroads.pop().unwrap();
            }
            // one way to go -> just continue
            // and increase steps since last crossroad
            else if avail_dirs == 1 {
                current_cell = next_cell;
            } 
            // multiple ways to go -> make this the last crossroad
            // and reset steps since prior crossroad
            // and go to the next cell
            else {
                crossroads.push(current_cell);
                steps = 0;
                current_cell = next_cell;
            }
            avail_dirs = 0;
        }

        self.solution_cache = path.clone();
        Ok(path)
    }
}
