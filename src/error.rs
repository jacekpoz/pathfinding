pub enum Error {
    NoStart, 
    TooManyStarts, 
    NoEnd, 
    TooManyEnds, 
    NoPath, 
}
