use crate::{cell::Cell, error::Error, pos::Pos};

pub trait Pathfinder {
    fn new(maze: &Vec<Vec<Cell>>) -> Self;

    fn set_maze(&mut self, maze: Vec<Vec<Cell>>);
    fn get_maze(&self) -> &Vec<Vec<Cell>>;

    fn solve(&mut self) -> Result<Vec<Pos>, Error>;

    fn is_empty(&self, pos: Pos) -> bool {
        let maze = self.get_maze();
        (pos.y as usize) < maze.len() && 
        (pos.x as usize) < maze[0].len() && 
        maze[pos.y as usize][pos.x as usize] != Cell::Wall
    }
}
