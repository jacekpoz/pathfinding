use crate::direction::Direction;

#[derive(PartialEq, Clone, Copy, Debug)]
pub struct Pos {
    pub x: i32, 
    pub y: i32, 
}

impl Pos {
    pub fn invalid() -> Pos {
        Pos {
            x: i32::min_value(), 
            y: i32::min_value()
        }
    }
}

impl std::ops::Add for Pos {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Self {
            x: self.x + rhs.x, 
            y: self.y + rhs.y
        }
    }
}

impl std::ops::AddAssign for Pos {
    fn add_assign(&mut self, rhs: Self) {
        *self = Self {
            x: self.x + rhs.x, 
            y: self.y + rhs.y
        }
    }
}

impl std::ops::Add<Direction> for Pos {
    type Output = Self;

    fn add(self, rhs: Direction) -> Self::Output {
        match rhs {
            Direction::Left  => { Self { x: self.x - 1, y: self.y     } }, 
            Direction::Up    => { Self { x: self.x,     y: self.y - 1 } }, 
            Direction::Right => { Self { x: self.x + 1, y: self.y     } }, 
            Direction::Down  => { Self { x: self.x,     y: self.y + 1 } }, 
        }
    }
}
