#[derive(PartialEq, Clone, Debug)]
pub enum Cell {
    Start, 
    End, 
    Path, 
    Wall, 
    Empty, 
}
