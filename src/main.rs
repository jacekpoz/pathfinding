mod algos;
mod cell;
mod direction;
mod error;
mod pathfinder;
mod pos;

use algos::*;
use cell::Cell;
use error::Error;
use pathfinder::Pathfinder;

use ansi_term::Colour;

/// pathfinding algorithms written by me cause why not
/// please don't use them in production
/// 0 - empty / path
/// 1 - wall
/// 2 - start
/// 3 - end
/// 4 - path chosen by algorithm

fn print_maze(maze: &Vec<Vec<Cell>>) {
    for row in maze {
        for cell in row {
            match cell {
                Cell::Empty => { print!("{}", Colour::Black.paint("██")); }, 
                Cell::Wall => { print!("{}", Colour::Yellow.paint("██")); }, 
                Cell::Start => { print!("{}", Colour::Green.paint("🚩")); }, 
                Cell::End => { print!("{}", Colour::Blue.paint("🏁")); }, 
                Cell::Path => { print!("{}", Colour::Red.paint("██")); }, 
            }
        }
        println!();
    }
}

fn convert_maze(old: &Vec<Vec<u8>>) -> Vec<Vec<Cell>> {
    let mut new: Vec<Vec<Cell>> = vec![];

    for row in old {
        let mut new_row: Vec<Cell> = vec![];
        for cell in row {
            match cell {
                0 => { new_row.push(Cell::Empty); }, 
                1 => { new_row.push(Cell::Wall);  }, 
                2 => { new_row.push(Cell::Start); }, 
                3 => { new_row.push(Cell::End);   }, 
                4 => { new_row.push(Cell::Path);  }, 
                _ => {}, 
            }
        }
        new.push(new_row);
    }

    new
}

fn main() {

    // change this to whatever you want, whatever size you want
    // might make a parser for some file with this in the future
    let maze: Vec<Vec<u8>> = vec![
        vec![2, 0, 0, 1, 0, 0, 0, 0, 0, 0], 
        vec![1, 0, 1, 1, 1, 1, 1, 1, 1, 0], 
        vec![1, 0, 0, 1, 0, 0, 1, 0, 0, 0], 
        vec![0, 1, 0, 1, 1, 0, 1, 0, 1, 0], 
        vec![0, 1, 0, 0, 1, 0, 1, 0, 1, 0], 
        vec![0, 1, 1, 0, 1, 0, 1, 0, 1, 1], 
        vec![0, 0, 1, 0, 1, 0, 0, 0, 0, 0], 
        vec![1, 0, 0, 0, 0, 1, 1, 0, 1, 0], 
        vec![1, 1, 1, 1, 0, 1, 1, 0, 1, 0], 
        vec![0, 0, 0, 0, 0, 0, 0, 0, 1, 3], 
    ];

    let new_maze = convert_maze(&maze);

    println!("maze: ");
    print_maze(&new_maze);

    let mut pathfinder = FirstThought::new(&new_maze);

    let result = pathfinder.solve();

    match result {
        Ok(path) => { 
            let mut solved_maze = new_maze.clone();
            for pos in path {
                let cell = &mut solved_maze[pos.y as usize][pos.x as usize];
                if *cell == Cell::Empty { *cell = Cell::Path; }
            }

            println!("found path: ");
            print_maze(&solved_maze);
        }, 
        Err(Error::NoStart) => { println!("no start in maze"); }, 
        Err(Error::TooManyStarts) => { println!("too many starts in maze"); }, 
        Err(Error::NoEnd) => { println!("no end in maze"); }, 
        Err(Error::TooManyEnds) => { println!("too many ends in maze"); }, 
        Err(Error::NoPath) => { println!("couldn't find a path"); }
    }
}

